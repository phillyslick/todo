require "bundler/capistrano"
require "rvm/capistrano"

# vagrant ip
server "192.168.33.10", :web, :app, :db, primary: true

set :application, "todo"
set :user, "vagrant"
set :deploy_to, "/home/#{user}/apps/#{application}"

# think about this one
set :deploy_via, :remote_cache
set :use_sudo, false
set :scm, "git"
set :repository, "git@bitbucket.org:phillyslick/todo.git"
set :branch, "master"
set :shared_children, shared_children + %w{public/uploads}

default_run_options[:pty] = true
set :ssh_options, {:forward_agent => true, keys: ['~/.vagrant.d/insecure_private_key']}


after "deploy", "deploy:cleanup" # keep only the last 5 releases

namespace :deploy do
  %w[start stop restart].each do |command|
    desc "#{command} unicorn server"
    task command, roles: :app, except: {no_release: true} do
      run "/etc/init.d/unicorn_#{application} #{command}"
    end
  end

  task :setup_config, roles: :app do
    sudo "ln -nfs #{current_path}/config/nginx.conf /etc/nginx/sites-enabled/#{application}"
    sudo "ln -nfs #{current_path}/config/unicorn_init.sh /etc/init.d/unicorn_#{application}"
    run "mkdir -p #{shared_path}/config"
    put File.read("config/database.yml"), "#{shared_path}/config/database.yml"
    put File.read("config/secrets.yml"), "#{shared_path}/config/secrets.yml"
  end
  after "deploy:setup", "deploy:setup_config"

  task :symlink_config, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/secrets.yml #{release_path}/config/secrets.yml"
  end
  
  task :seed_db, roles: :app do
    run "cd #{current_path}; bundle exec rake db:seed RAILS_ENV=#{rails_env}"
  end
  
  after "deploy:finalize_update", "deploy:symlink_config"
  
  # Don't want to accidentially seed the db.
  #after "deploy:cold", "deploy:seed_db"

  desc "Make sure local git is in sync with remote."
  task :check_revision, roles: :web do
    unless `git rev-parse HEAD` == `git rev-parse origin/master`
      puts "WARNING: HEAD is not the same as origin/master"
      puts "Run `git push` to sync changes."
      exit
    end
  end
  before "deploy", "deploy:check_revision"
end